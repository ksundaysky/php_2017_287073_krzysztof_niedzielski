<title>Register</title>

<h2>Register</h2>
<ul>
<form method="post" action="/auth/store">
    <li>id<input type="number" name="id"
        value="<?php if(isset($_SESSION['id']))
            echo $_SESSION['id']?>"</li>
    <li>name<input type="text" name="name" value="<?php if(isset($_SESSION['name']))
            echo $_SESSION['name']?>"/></li>

    <li>surname<input type="text" name="surname" value="<?php if(isset($_SESSION['surname']))
            echo $_SESSION['surname']?>" /></li>
    <li>email<input type="text" name="email" value="<?php if(isset($_SESSION['email']))
            echo $_SESSION['email']?>"/></li>
    <li>password<input type="text" name="password" value="<?php if(isset($_SESSION['password']))
            echo $_SESSION['password']?>"/></li>
    <li>password confirmation<input type="text" name="password_confirmation" value="<?php if(isset($_SESSION['password_confirmation']))
            echo $_SESSION['password_confirmation']?>" /></li>
<!--    <li><input type="text" name="confirmed" /><li>-->


        <input type="submit" value="Create">
</form>
</ul>


<ul>
    <?php if(empty($_SESSION["id"])&&isset($_SESSION["id"]))
        echo "<li class='error'>The id filed cannot be empty</li>"?>
    <?php if(empty($_SESSION["name"])&&isset($_SESSION["name"]))
        echo "<li class='error'>The name filed cannot be empty</li>"?>
    <?php if(empty($_SESSION["surname"])&&isset($_SESSION["surname"]))
        echo "<li class='error'>The surname filed cannot be empty</li>"?>
    <?php if(empty($_SESSION["email"])&&isset($_SESSION["email"]))
        echo "<li class='error'>The email filed cannot be empty</li>"?>
    <?php if(empty($_SESSION["password"])&&isset($_SESSION["password"]))
        echo "<li class='error'>The password filed cannot be empty</li>"?>
    <?php if(empty($_SESSION["password_confirmation"])&&isset($_SESSION["password_confirmation"]))
        echo "<li class='error'>The password confirmation filed cannot be empty</li>"?>
    <?php if(empty($_SESSION["pass"])&&isset($_SESSION["pass"]))
        echo "<li class='error'>The password confirmation filed does not match the password field</li>"?>
</ul>

<?php session_unset()?>
