<h2>Comments</h2>

<a href="/comments/create">Create comment...</a>

<ul>
    <?php foreach ($comments as $comment) { ?>
    <li><a href="/comments/<?= $comment->id() ?>"><?= $comment->text ?></a></li>
    <?php } ?>
</ul>
