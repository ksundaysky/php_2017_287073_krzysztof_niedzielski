<?php

namespace Storage;


use Concept\Distinguishable;

use Predis\Client;

class RedisStorage implements Storage
{
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function store(Distinguishable $distinguishable)
    {
        $this->client->set($distinguishable->key(), serialize($distinguishable));
    }

    public function loadAll(): array
    {
        $keys = $this->client->keys("*");
        $objects = $this->client->mget($keys);

        $result = [];

        foreach ($objects as $object) {
            $result[] = unserialize($object);
        }

        return $result;
    }

    public function load(string $pattern): array
    {
        throw new \Exception("load(pattern) function not yet implemented!");
    }

    public function remove(string $pattern)
    {
        throw new \Exception("remove() function not yet implemented!");
    }
}