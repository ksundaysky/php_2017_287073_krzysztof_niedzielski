<?php

namespace Container;

class RingBuffer
{
    private $capacity = null;
    private $size = 0;
    private $array =  array();
    public function __construct($capacity){

        $this->capacity=$capacity;
    }

    public function size() {
        return $this->size;
    }

    public function empty() {
        if($this->size != 0) return false;
        return true;
    }

    public function capacity() {
        return $this->capacity;
    }

    public function push($value) {

        if($this->size == $this->capacity){
            array_shift($this->array);
            array_push($this->array,$value);
        }
        else{
        array_push($this->array,$value);
        $this->size+=1;}
    }

    public function full() {
        if($this->size >= $this->capacity) return true;
        return false;
    }

    public function pop() {
        if($this->size > 0){
            $this->size--;
            return array_shift($this->array);
        }

        return null;
    }

    public function head() {
        if($this->size > 0) {
            return end($this->array);
        }
        return null;
    }

    public function tail() {
        if($this->size > 0) {
            return reset($this->array);
        }
        return null;
    }

    public function at($var) {
        if($var >=$this->size){
            return null;
        }
        return $this->array[$var];
    }

}