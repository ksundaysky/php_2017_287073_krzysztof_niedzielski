<?php

namespace Controller;

use Model\Comment;

class CommentsController extends Controller
{
    public function index()
    {
        $comments = $this->getStorage()->load("model_comment_*");

        return ["comments/index", ["comments" => $comments]];
    }

    public function create()
    {
        return "comments/create";
    }

    public function store()
    {
        $comment = new Comment($_POST["id"]);
        $comment->text = $_POST["text"];

        $this->getStorage()->store($comment);

        header("Location: /comments");
    }

    public function show($id)
    {
        $comments = $this->getStorage()->load("model_comment_" . $id);

        return ["comments/show", ["comment" => $comments[0]]];
    }

    public function delete($id)
    {
        $this->getStorage()->remove("model_comment_" . $id);

        header("Location: /comments");
    }
}