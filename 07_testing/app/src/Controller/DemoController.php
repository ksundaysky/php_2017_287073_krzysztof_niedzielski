<?php

namespace Controller;

use Storage\MySQLStorage;

use Widget\Button;
use Widget\Link;

use Log;
use Scene;

class DemoController extends Controller
{
    public function index()
    {

        Log::info("MySQL Storage Demo");

        $storage = new MySQLStorage();

        $storage->store(new Button(1));
        $storage->store(new Button(2));
        $storage->store(new Link(3));

        $widgets = $storage->loadAll();

        $scene = new Scene();

        return ["demo", ["widgets" => $widgets, "scene" => $scene]];
    }
}