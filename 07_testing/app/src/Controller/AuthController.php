<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 09.12.17
 * Time: 13:18
 */

namespace Controller;

use Model\User;
class AuthController extends  Controller
{
    public function index()
    {
        $auth = $this->getStorage()->load("model_user_*");

        return ["auth/index", ["auth" => $auth]];
    }

    public function create()
    {
        return "comments/create";
    }

    public function store()
    {
        $var=false;
        if(empty($_POST['id']))
        {
            $var=true;
            $_SESSION["id"]=$_POST['id'];
            header("Location: /auth/register");

        }
        else{

            $_SESSION['id']=$_POST['id'];
            header("Location: /auth/register");
        }
        if(empty($_POST['name']))
        {
            $var=true;
            $_SESSION["name"]=$_POST['name'];
            header("Location: /auth/register");
        }
        else{

            $_SESSION['name']=$_POST['name'];
            header("Location: /auth/register");
        }
        if(empty($_POST['surname']))
        {
            $var=true;
            $_SESSION["surname"]=$_POST['surname'];
            header("Location: /auth/register");
        }
        else{

            $_SESSION['surname']=$_POST['surname'];
            header("Location: /auth/register");
        }

//        if(empty($_POST['surname']))
//        {
//            $var=true;
//            $_SESSION["surname"]=$_POST['surname'];
//            header("Location: /auth/register");
//        }
        if(empty($_POST['email']))
        {
            $var=true;
            $_SESSION["email"]=$_POST['email'];
            header("Location: /auth/register");
        }
        else{

            $_SESSION['email']=$_POST['email'];
            header("Location: /auth/register");
        }
        if(empty($_POST['password']))
        {
            $var=true;
            $_SESSION["password"]=$_POST['password'];
            header("Location: /auth/register");
        }
        if(empty($_POST['password_confirmation']))
        {
            $var=true;
            $_SESSION["password_confirmation"]=$_POST['password_confirmation'];
            header("Location: /auth/register");
        }
        if($_POST["password"] !=  $_POST["password_confirmation"]){

            $var=true;
            $_SESSION["pass"] = "";
            header("Location: /auth/register");
            exit();

        }

        if($var){
            header("Location: /auth/register");
            exit();
        }

        else {


            $auth = new User($_POST["id"]);

            $auth->id = $_POST["id"];

            $auth->name = $_POST["name"];
            $auth->surname = $_POST["surname"];
            $auth->email = $_POST["email"];
            $auth->password = $_POST["password"];
            $auth->password_confirmation = $_POST["password_confirmation"];
            $auth->password = password_hash($auth->password, PASSWORD_DEFAULT);

            $auth->token = md5(openssl_random_pseudo_bytes(32));


            //if ($_POST["password"] == $_POST["password_confirmation"]) {
                $auth->confirmed = false;
            //} else


            $this->getStorage()->store($auth);



            header("Location: /auth/confirmation_notice");

            exit();
            //return $this->confirmation_notice();
        }


        header("Location: /auth/register");
    }

    public function show($id)
    {
        $auth = $this->getStorage()->load("model_user_" . $id);

        return ["auth/show", ["auth" => $auth[0]]];
    }

    public function delete($id)
    {
        $this->getStorage()->remove("model_comment_" . $id);

        header("Location: /comments");
    }

    public function register(){
        return "auth/register";
    }

    public function login(){


        return "auth/login";
    }

    public function confirmationlogin(){

        $auth = $this->getStorage()->load("model_user_*");
        $_SESSION['email'] = $_POST['email'];

        $var = true;

        foreach($auth as $user){

            if($user->email == $_POST['email']){
                $var = false;

                if(password_verify($_POST['password'],$user->password)){
                    if($user->confirmed ==false) {
                        header("Location: /auth/confirmation_notice");
                        exit();
                    }
                    else {
                        $_SESSION['username'] = $user->name;
                        $_SESSION['surname'] = $user->surname;
                        $_SESSION['loggingSuccesful'] = true;
                        header("Location: /");
                        exit();
                    }

                }
                else{

                    $_SESSION['loggingFailure'] = true;
                    header("Location: /auth/login");
                    exit();
                }

            }

        }


        if($var){

            $_SESSION['loggingFailure'] = true;
            header("Location: /");
            exit();
        }


        return $this->confirmation_notice();
    }

    public function confirmation_notice(){
        return "auth/confirmation_notice";
    }

    public function logout(){
        $_SESSION['loggedOut']=true;
        header("Location: /");
    }


    public function confirm($var){ //confirm/1a
        $auth = $this->getStorage()->load("model_user_*");
        foreach($auth as $user){

            if($var == $user->token ) {

                $user->confirmed = true;
                $user->token=null;
                header("Location: /");
                $_SESSION['provideToken'] = true;
                $this->getStorage()->store($user);

            }
            else {
                $_SESSION['errorToken'] = true;

                header("Location: /");
            }
        }

    }



}