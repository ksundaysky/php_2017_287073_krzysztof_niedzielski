<?php

namespace Controller;


use Storage\Storage;

class Controller
{

    /**
     * @var Storage
     */
    private $storage;

    /**
     * @return Storage
     */
    public function getStorage(): Storage
    {
        return $this->storage;
    }

    /**
     * @param Storage $storage
     */
    public function setStorage(Storage $storage)
    {
        $this->storage = $storage;
    }
}