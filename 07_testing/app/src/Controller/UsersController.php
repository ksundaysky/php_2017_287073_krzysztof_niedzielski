<?php

namespace Controller;

class UsersController extends Controller
{
    private $users;

    public function __construct()
    {
        $this->users = [
            1 => [
                'name' => 'John',
                'surname' => 'Doe',
                'age' => 21
            ],
            2 => [
                'name' => 'Peter',
                'surname' => 'Bauer',
                'age' => 16
            ],
            3 => [
                'name' => 'Paul',
                'surname' => 'Smith',
                'age' => 18
            ]
        ];

    }

    public function index()
    {
        return ["users", ["users" => $this->users]];
    }

    public function show($id)
    {
        return ["user", ["user" => $this->users[$id]]];
    }
}