<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 09.12.17
 * Time: 13:11
 */

namespace Model;

use Concept\Distinguishable;

class User extends Distinguishable
{
    public $id;
    public $name;
    public $surname;
    public $email;
    public $password;
    public $password_confirm;
    public $confirmed;
    public $token;

}