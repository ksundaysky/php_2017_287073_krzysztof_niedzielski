<?php

use Config\Directory;

class App {

    public function run()
    {
        session_start();

        $parts = explode('/', $_SERVER['REQUEST_URI']);
        array_shift($parts);


        $option = $parts[0] ?? 'home' ?: 'home';


        $controllerClassName = "Controller\\" . ucwords($option) . "Controller";

        if (class_exists($controllerClassName)) {

            $controller = new $controllerClassName;

            if (method_exists($controller, "setStorage")) {
                $controller->setStorage(new Storage\MySQLStorage());
            }

            $controllerMethodName = $parts[1] ?? 'index' ?: 'index';
            $controllerMethodArguments = array_slice($parts, 2);

            if (is_numeric($controllerMethodName)) {
                $controllerMethodName = "show";
                $controllerMethodArguments = array_slice($parts, 1);
            }

            if (method_exists($controller, $controllerMethodName)) {

                $result = call_user_func_array([$controller, $controllerMethodName], $controllerMethodArguments);

            } else {

                $result = "404";
            }

        } else {
            $result = "404";
        }

        if (is_array($result)) {
            $view = $result[0];
            $data = $result[1];
        } else {
            $view = $result;
            $data = [];
        }

        extract($data);

        require_once(Directory::view() . 'layout.php');
    }
}
