<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 06.12.17
 * Time: 16:45
 */

namespace String;


class Editor
{


   private $text= null;
    public function __construct($text)
    {
        $this->text=$text;
    }




    public function get()
    {
        return $this->text;
    }

    public function replace($first,$second)
    {

        $this->text=str_replace($first,$second,$this->text);

        return $this;
    }


    public function lower()
    {
        $this->text=strtolower($this->text);

        return $this;
    }

    public function upper()
    {
        $this->text=strtoupper($this->text);

        return $this;
    }


    public function censor($var1){


         $var2 = str_repeat('*', strlen($var1));
         $this->text=str_replace($var1,$var2,$this->text);

        return $this;
    }

    public function repeat($word,$number)
    {
        $var2=str_repeat($word,$number);
        $this->text=str_replace($word,$var2,$this->text);

        return $this;
    }
    public function remove($word)
    {
        //$var2=str_repeat($word,$number);
        $this->text=str_replace($word,"",$this->text);

        return $this;
    }
}