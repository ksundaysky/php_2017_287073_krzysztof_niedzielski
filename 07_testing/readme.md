# PHP Application Testing - Codeception

## What to do?

1) Copy this project to your repository (this directory 07_testing)

2) Setup test environment using below instruction

3) Fix unit test by implementing Container\RingBuffer and String\Editor classes

4) Fix acceptance test by adding appropriate controllers/views/models etc.

5) Commit all your changes to **app/** directory under **07_testing/**

When grading projects, directory **app/** from your repo will be copied and all test will be executed on it.
Any other changes will be ignored! Make sure that you change only that directory!


#### Points (0-3)

There are two basic points to get if all test are green.
If there is single test case failing you can still get one point.

There is also one bonus point for exceptionally clean/pretty code!

#### Deadline

Deadline to submit all your changes: **10.12.2017** (including that day).


### How to install dependencies?

```
composer install
```


### How to setup storage permissions?

```
chmod -R 0777 storage/
```

### How to setup server?

```

# Switch to root account
sudo su -

# Go to Nginx configuration directory
cd /etc/nginx/sites-available/

# Copy example configuration from project directory
cp /home/student/Phpstorm/07_testing/nginx/07_testing .

# Go to enabled configuration directory
cd /etc/nginx/sites-enabled/

# Enable new configuration
ln -s /etc/nginx/sites-available/07_testing .

# Restart server
service nginx restart

# Test whether it works in browser or using e.g.: curl
curl localhost:8007

# You should see some HTML in console output

# Exeit from root session
exit
```

# Codeception

### 1. How to add Codeception as development dependency?

```bash
composer require codeception/codeception --dev
```

### 2. How to bootstrap codeception tests?

```bash
vendor/bin/codecept bootstrap
```

### 3. How to run tests?

```bash
# all
vendor/bin/codecept run

# or specific suite
vendor/bin/codecept run unit
vendor/bin/codecept run functional
vendor/bin/codecept run acceptance
```

### 4. How to add unit test?

```bash
vendor/bin/codecept generate:test unit Dummy
# creates tests/unit/DummyTest.php

vendor/bin/codecept generate:cest unit Dummy
# creates tests/unit/DummyCest.php  

vendor/bin/codecept generate:cept unit Dummy
# creates tests/unit/DummyCept.php
```

### 5. Add test for class inside some namespace

```bash
vendor/bin/codecept generate:cest unit Container_RingBuffer
# Why 'cest' type - I think it best fits UT
```

### 6. Add acceptance test

```bash
vendor/bin/codecept generate:cept acceptance Homepage
# Again - 'cept' looks better in acceptance tests
```

### 7. How to set URL for acceptance tests?

Edit file **tests/acceptance.suite.yml**

```yaml
modules:
    enabled:
        - PhpBrowser:
            url: http://localhost:8007 # <-- fixed address here
        - \Helper\Acceptance
```

### 8. Configure for database testing

Edit file [tests/acceptance.suite.yml](tests/acceptance.suite.yml)

```yaml
actor: AcceptanceTester
modules:
    enabled:
        - PhpBrowser:
            url: http://localhost:8007
        - \Helper\Acceptance
        - Db: # <-- added this section
            dsn: 'mysql:host=localhost;dbname=test'
            user: 'test'
            password: 'test123'
```

### 9. How to generate database dump?

```bash
mysqldump -u test test -p > tests/_data/dump.sql
```

### 10. How to use dump to initialize DB state?

Edit file [tests/acceptance.suite.yml](tests/acceptance.suite.yml)

```yaml
actor: AcceptanceTester
modules:
    enabled:
        - PhpBrowser:
            url: http://localhost:8007
        - \Helper\Acceptance
        - Db:
            dsn: 'mysql:host=localhost;dbname=test'
            user: 'test'
            password: 'test123'
            populate: true # <-- added this
            cleanup: true # <-- added this
            dump: tests/_data/dump.sql # <-- added this
```

### 11. How to switch to WebDriver?

```yaml
actor: AcceptanceTester
modules:
    enabled:
#        - PhpBrowser:
#            url: http://localhost:8007
        - WebDriver: # <-- using it instead of PhpBrowser
            url: http://localhost:8007
            browser: chrome
        - \Helper\Acceptance
        - Db:
            dsn: 'mysql:host=localhost;dbname=test'
            user: 'test'
            password: 'test123'
            populate: true
            cleanup: true
            dump: tests/_data/dump.sql
```

### 12. How to run Selenium?

Open new terminal window and execute:

```bash
cd Selenium/
java -jar selenium-server-standalone.jar 
```